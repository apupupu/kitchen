class AddNewRecipeColumns < ActiveRecord::Migration[6.0]
  def change
    add_column :recipes, :published_at, :datetime
    add_column :recipe_ingredients, :importance, :integer
    add_column :recipe_ingredients, :option_group, :integer
  end
end
