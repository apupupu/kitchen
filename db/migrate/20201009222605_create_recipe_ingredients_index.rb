class CreateRecipeIngredientsIndex < ActiveRecord::Migration[6.0]
  def change
    execute <<-SQL
      create unique index uidx_recipe_ingredient on recipe_ingredients (recipe_id, ingredient_id);
    SQL
  end
end
