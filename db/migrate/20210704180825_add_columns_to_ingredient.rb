class AddColumnsToIngredient < ActiveRecord::Migration[6.0]
  def change
    add_column :ingredients, :description, :text
    add_column :ingredients, :created_by, :integer
    add_column :ingredients, :updated_by, :integer
    add_column :ingredients, :deleted, :boolean, null: false, default: false
    add_column :ingredients, :variant_of, :integer
    add_foreign_key :ingredients, :users, column: :created_by
    add_foreign_key :ingredients, :users, column: :updated_by
    add_foreign_key :ingredients, :ingredients, column: :variant_of
  end
end
