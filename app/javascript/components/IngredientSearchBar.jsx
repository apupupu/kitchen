import React from "react"
import Autosuggest from 'react-autosuggest';

class IngredientSearchBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      suggestions: [],
      // list of { id: int, ingredientIndex: int, name: string }
      ingredients: null,
    };
  }

  render() {
    const { value, suggestions } = this.state;

    // Autosuggest will pass through all these props to the input.
    const inputProps = {
      placeholder: 'Search ingredients',
      value,
      onChange: this.onChange
    };

    return (
      <Autosuggest
        suggestions={suggestions}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        getSuggestionValue={this.getSuggestionValue}
        renderSuggestion={this.renderSuggestion}
        inputProps={inputProps}
        onSuggestionSelected={this.selectItem}
      />
    );
  }

  selectItem = (event, data) => {
    const { suggestion } = data;

    this.setState(prevState => {
      let { value } = { ...prevState };
      value = "";
      return { value: value };
    });

    if (this.props.addItemCallback) {
      this.props.addItemCallback(suggestion.id, suggestion.name);
    }
  }

  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    });
  };

  renderSuggestion = (suggestion) => {
    return <div>
      {suggestion.name}
    </div>;
  };
  
  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: this.getSuggestions(value)
    });
  };

  getSuggestionValue = (suggestion) => {
    return suggestion.name
  };

  getSuggestions = (value) => {
    const inputValue = value.trim().toLowerCase();

    return inputValue.length === 0 ? [] : this.state.ingredients.filter(o => {
      return o.name.toLowerCase().indexOf(inputValue) > -1 && !this.props.hiddenIngredients.some(other => other.ingredient_id === o.id);
    }
    );
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  updateIngredients = (ingredients) => {
    ingredients.forEach((o, i) => {
      o.ingredientIndex = i;
    });
    this.setState({ ingredients: ingredients });
  }

  componentDidMount() {
    $.getJSON('/ingredients/list', this.updateIngredients);
  }

}
export default IngredientSearchBar
