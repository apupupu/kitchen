import React from "react"
import IngredientSearchBar from './IngredientSearchBar.jsx';

class IngredientList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      ingredients: this.props.initialIngredients ? this.props.initialIngredients.slice() : [],
    };

    this.importances = [
      [0, "-"],
      [1, "Suggested"],
      [2, "Recommended"],
      [3, "Normal"],
      [4, "Important"],
      [5, "Critical"],
    ];
  }

  render() {
    return (
      <div>
        {this.props.editable ? <IngredientSearchBar
          addItemCallback={this.addItem}
          hiddenIngredients={this.state.ingredients}
        >
        </IngredientSearchBar> : null}
        {this.state.ingredients.length > 0 ? <table>
          <thead>
            <tr>
              <th>Ingredient</th>
              {this.props.showAmount ? <th>Amount</th> : null}
              {this.props.showAmount ? <th>Importance</th> : null}
              {this.props.editable ? <th></th> : null}
            </tr>
          </thead>
          <tbody>
            {this.state.ingredients.map((o, i) => {
              return <tr key={o.ingredient_id}>
                  <td>
                    {o.name}
                    <input type="hidden" name="recipe[ingredients][][ingredient_id]" value={o.ingredient_id} />
                  </td>
                  {this.props.showAmount ? <td>
                    {this.props.editable ?
                      <input type="text" name="recipe[ingredients][][amount]" value={o.amount || ''} onChange={this.setAmount.bind(this, i)} />
                      : o.amount }
                  </td> : null}
                  {this.props.showAmount ? <td>
                    {this.props.editable ?
                      <select name="recipe[ingredients][][importance]" value={o.importance || 0} onChange={this.setImportance.bind(this, i)}>
                        {this.importances.map(o => <option value={o[0]} key={o[0]} selected={o.importance === o[0] || null} >
                          {o[1]}
                        </option>)}
                      </select>
                      : this.importances[o.importance || 0][1] }
                  </td> : null}
                  {this.props.editable ? <td>
                      <input type="button" onClick={this.removeItem.bind(this, i)} value="Remove" />
                    </td> : null}
                </tr>;
            })}
          </tbody>
        </table> : null}
      </div>
    );
  }

  addItem = (ingredient_id, name) => {
    const ingredient = {
      ingredient_id: ingredient_id,
      name: name,
    }
    this.setState((prevState) => {
      let { ingredients } = { ...prevState };
      ingredients.push(ingredient);
      return { ingredients: ingredients };
    })
  }

  removeItem = (ingredientIndex, evt) => {
    this.setState((prevState) => {
      let { ingredients } = { ...prevState };
      ingredients.splice(ingredientIndex, 1);
      return { ingredients: ingredients };
    })
  }

  setAmount = (ingredientIndex, evt) => {
    const newVal = evt.target.value;
    this.setState((prevState) => {
      let { ingredients } = { ...prevState };
      ingredients[ingredientIndex].amount = newVal;
      return { ingredients: ingredients };
    })
  }

  setImportance = (ingredientIndex, evt) => {
    const newVal = evt.target.value;
    this.setState((prevState) => {
      let { ingredients } = { ...prevState };
      ingredients[ingredientIndex].importance = newVal;
      return { ingredients: ingredients };
    })
  }

  getIngredientIds = () => {
    return this.state.ingredients.map((o, i) => o.ingredient_id);
  }

}
export default IngredientList
