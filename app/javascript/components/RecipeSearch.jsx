import React from "react"
import IngredientList from './IngredientList.jsx'

class RecipeSearch extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      results: [],
    };

    this.listRef = React.createRef();
  }

  render() {
    return (
      <form onSubmit={this.search}>
        <div>
          <h3>Containing ingredients</h3>
          <IngredientList editable={true} ref={this.listRef} />
        </div>
        <input type="button" value="Search" onClick={this.search} />
        { this.state.results.length > 0 ? <div>
          <h3>Search results</h3>
          <table className="recipes-table">
            <thead>
              <tr>
                <th>Name</th>
              </tr>
            </thead>
            <tbody>
              { this.state.results.map((r, i) => {
                return <tr key={r.id}>
                  <td>
                    <a href={`/recipe/${r.id}`}>{r.name}</a>
                  </td>
                </tr>;
              })}
            </tbody>
          </table>
        </div> : <div>No recipes found</div> }
      </form>
    );
  }

  search = (evt) => {
    $.getJSON(`/recipes/search?ingredients=${this.listRef.current.getIngredientIds()}`, this.updateResults);
  }

  updateResults = (results) => {
    this.setState({ results: results });
  }

}
export default RecipeSearch
