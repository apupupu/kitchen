class IngredientService

  def update(id, user_id, name, description = nil, variant_of = nil)
    ingredient = Ingredient.find(id)
    ingredient.update(
      :updated_by => user_id,
      :name => name,
      # :description => description,
      # :variant_of => variant_of,
    )
  end


  def delete(id, user_id)
    ingredient = Ingredient.find(id)
    ingredient.update(
      :updated_by => user_id,
      :deleted => true,
    )
  end

end