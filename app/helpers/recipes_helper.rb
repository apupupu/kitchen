module RecipesHelper

  def update_recipe_ingredients(ingredients)
    return true if ingredients.nil?
    ingredients.each do |o|
      if o[:importance].to_i < 1 or o[:importance].to_i > 5
        o[:importance] = nil
      end
    end
    success = true
    success &&= RecipeIngredient.where(recipe_id: @recipe.id).where.not(ingredient_id: ingredients.map{|o| o[:id]}).destroy_all
    if !ingredients.empty?
      success &&= RecipeIngredient.where(recipe_id: @recipe.id).where(ingredient_id: ingredients.map{|o| o[:id]}).upsert_all(
        ingredients.map{|o| o.to_h.merge(
          recipe_id: @recipe.id,
          created_at: Time.now,
          updated_at: Time.now,
        )},
        unique_by: [:recipe_id, :ingredient_id]
      )
    end
    return success
  end

  def search(ingredients)
    q = <<-SQL
      select r.id, r.name
      from recipes r
      inner join (
        select ri.recipe_id as recipe_id, jsonb_agg(i.id) as ingredient_ids
        from recipe_ingredients ri
        inner join ingredients i
        on ri.ingredient_id = i.id
        group by ri.recipe_id
      ) ris
      on ris.recipe_id = r.id
      where ris.ingredient_ids @> jsonb_build_array(:ingredients)
    SQL
    Recipe.find_by_sql([q, :ingredients => ingredients])
  end

end
