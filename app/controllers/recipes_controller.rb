class RecipesController < ApplicationController

  before_action :authenticate_user!, except: [:index, :show, :search]

  def index
    @recipes = Recipe.where.not(published_at: nil)
  end

  def show
    @recipe = Recipe.find(params[:id])
    @ingredients = Recipe.joins("LEFT JOIN recipe_ingredients ri on ri.recipe_id = recipes.id")
    .joins("inner join ingredients i on i.id = ri.ingredient_id")
    .select("ri.id, ri.amount, ri.importance, ri.ingredient_id, i.name")
    .where('recipes.id' => params[:id])
  end

  def new
    @recipe = Recipe.new()
  end

  def edit
    @recipe = Recipe.find(params[:id])
    @ingredients = Recipe.joins("LEFT JOIN recipe_ingredients ri on ri.recipe_id = recipes.id")
      .joins("inner join ingredients i on i.id = ri.ingredient_id")
      .select("ri.id, ri.amount, ri.importance, ri.ingredient_id, i.name")
      .where('recipes.id' => params[:id])
  end

  def update
    @recipe = Recipe.find(params[:id])

    p = recipe_params

    success = @recipe.update(
      name: p[:name],
      text: p[:text],
    )
    success &&= helpers.update_recipe_ingredients(p[:ingredients])

    if success
      redirect_to(@recipe)
    else
      render(:edit)
    end
  end

  def create
    p = recipe_params

    @recipe = Recipe.new(
      name: p[:name],
      text: p[:text],
      author_id: current_user.id,
    )
    success = @recipe.save()
    success &&= helpers.update_recipe_ingredients(p[:ingredients])

    if success
      redirect_to(@recipe)
    else
      render(:new)
    end
  end
  
  def destroy
    @recipe = Recipe.find(params[:id])

    if !@recipe.published_at.nil?
      @recipe.errors.add(:base, :recipe_published, message: "Cannot delete published recipe")
      render(:show) and return
    end

    @recipe.destroy
   
    redirect_to(recipes_path)
  end

  def search
    p = search_params
    ingredients = p[:ingredients].to_s.split(",").map{|o| o.to_i}
    render(:json => helpers.search(ingredients))
  end

  def publish
    @recipe = Recipe.find(params[:id])

    if @recipe.ingredients.blank?
      @recipe.errors.add(:base, :no_ingredients, message: "Cannot publish recipe with no ingredients")
      render(:show)
    else
      @recipe.update(published_at: Time.now())
      redirect_to(@recipe)
    end
  end

  private

  def recipe_params
    params.require(:recipe).permit(:name, :text, ingredients: [:ingredient_id, :amount, :importance])
  end

  def search_params
    params.permit(:name, :ingredients)
  end

end
