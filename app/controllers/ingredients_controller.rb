class IngredientsController < ApplicationController

  before_action :authenticate_user!, except: [:index, :list]

  def index
    @ingredient = Ingredient.new()
    @ingredients = Ingredient.all.order(:name)
  end


  def list
    @ingredients = Ingredient.all
    return render(:json => @ingredients)
  end


  def show
    @ingredient = Ingredient.find(params[:id])
  end


  def edit
    @ingredient = Ingredient.find(params[:id])
  end


  def create
    s = IngredientService.new
    @ingredient = s.create(current_user.id, params[:name])

    if @ingredient.save()
      redirect_to(action: :index)
    else
      @ingredients = Ingredient.all
      render(:index)
    end
  end


  def update
    @ingredient = Ingredient.find(params[:id])

    begin
      s = IngredientService.new
      s.update(params[:id], current_user.id, params[:ingredient][:name])
      redirect_to ingredients_path()
    rescue Exception => e
      render(:edit)
    end
  end


  private


  def ingredient_params
    params.require(:ingredient).permit(:name)
  end

end
