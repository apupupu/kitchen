Rails.application.routes.draw do
  get 'welcome/index'

  get '/recipes', to: 'recipes#index', as: 'recipes'
  post '/recipes', to: 'recipes#create'
  get '/recipes/new', to: 'recipes#new', as: 'new_recipe'
  get '/recipe/:id/edit', to: 'recipes#edit', as: 'edit_recipe'
  get '/recipe/:id', to: 'recipes#show', as: 'recipe'
  delete '/recipe/:id', to: 'recipes#destroy'
  patch '/recipe/:id', to: 'recipes#update'
  post '/recipe/:id', to: 'recipes#publish', as: 'publish_recipe'
  get '/recipes/search', to: 'recipes#search'

  get '/ingredients', to: 'ingredients#index', as: 'ingredients'
  get '/ingredients/list', to: 'ingredients#list', as: 'ingredients_list'
  get '/ingredients/show/:id', to: 'ingredients#show', as: 'ingredients_show'
  get '/ingredients/edit/:id', to: 'ingredients#edit', as: 'ingredients_edit'
  post '/ingredients/create', to: 'ingredients#create', as: 'ingredients_create'
  patch '/ingredients/update/:id', to: 'ingredients#update', as: 'ingredients_update'

  get '/profile/:id', to: 'profiles#show', as: 'profile'

  root 'welcome#index'

  devise_for :users

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
